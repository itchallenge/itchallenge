<?php 
require_once 'resources/init.php';
$user = User::getInstance();

if($user->isLogin() && $user->getRole() === 'admin') {
	$pageBuilder = new PageBuilder();
	$pageBuilder->buildPage();
} else {
	header('Location: index.php');
}
