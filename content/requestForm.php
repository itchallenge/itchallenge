<script src="<?=Config::JAVASCRIPTPATH?>events.js"></script>
<form action="<?=Config::RESOURCESPATH?>requestEventCode.php" method="post" id="requestForm" class="fontFiraSans">

<!-- Existing event -->
<select name="runningEventCategory" class="selector" id="runningEventCategoryPicker" class="input">
</select>
<br>

<select name="runningEvent" class="selector" id="runningEventPicker" class="input">
</select>
<br>



<br>
<label for="newEventCheckbox">Maak een nieuwe aanvraag:</label>
<input type="checkbox" name="newEventCheckbox" id="newEventCheckbox" class="input">

<!-- new event from existing event -->
<br>
<select name="eventCategory" class="selector" id="eventCategoryPicker" class="input">
</select>
<br>

<select name="event" class="selector" id="eventPicker" class="input">
</select>
<br>
<br>
<label class="label" for="startEvent">Start datum van de Opleiding/Cursus:</label>
<input type="date" id="startEvent" name="startEvent"
min="<?php echo(date('Y-m-d')) ?>">
<br>
<label class="label" for="endEvent">Eind datum van de Opleiding/Cursus:</label>
<input type="date" id="endEvent" name="endEvent"
min="<?php echo(date('Y-m-d')) ?>">
<br>
<label class="label" for="deadline">Deadline incshrijven van de Opleiding/Cursus:</label>
<input type="date" id="deadline" name="deadline"
min="<?php echo(date('Y-m-d')) ?>">
<br>
<br>

<!-- completely new event -->


<label class="label" for="eventCheckbox">Anders:</label>
<input type="checkbox" name="eventCheckbox" id="eventCheckbox" class="input">
<br>
<label class="label" for="eventName">Naam Opleiding/Cursus:</label>
<input type="text" name="eventName" id="eventName" class="input">
<br>
<label class="label" for="eventLocation">Locatie:</label>
<input type="text" name="eventLocation" id="eventLocation" class="input">
<br>
<label class="label" for="eventPrice">Kosten:</label>
<input type="text" name="eventPrice" id="eventPrice" class="input">
<br>
<br>
<input type="submit" value="Vraag aan" name="submit" class="inputButton fontRobotoBold">


</form>