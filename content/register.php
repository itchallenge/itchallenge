<form class="fontFiraSans" action="<?=Config::RESOURCESPATH?>registerCode.php" method="post">
<h1 class="fontFiraSansTitle" float: centre >Regristreren nieuwe gebruiker</h1>
<br>
	<input class="input" type="text" name="first_name" placeholder="Voornaam"
	<?php 
	if(isset($_SESSION['tempfirstName'])) {
		echo('value="');
		echo($_SESSION['tempfirstName']);
		echo('"');
	}
	?>><br>
	<input class="input" type="text" name="last_name" placeholder="Achternaam"
	<?php
	if(isset($_SESSION['templastName'])) {
		echo('value="');
		echo($_SESSION['templastName']);
		echo('"');
	}
	?>><br>
	<input class="input" type="text" name="username" placeholder="Gebruikersnaam"
	<?php
	if(isset($_SESSION['tempusername'])) {
		echo('value="');
		echo($_SESSION['tempusername']);
		echo('"');
	}
	?>><br>
	<input class="input" type="password" name="password" placeholder="Wachtwoord"><br>
	<input class="input" type="password" name="password_confirm" placeholder="Bevestig Wachtwoord"><br>
	<input class="input" type="text" name="supervisor" placeholder="Leidinggevende"
	<?php
	if(isset($_SESSION['tempsupervisor'])) {
		echo('value="');
		echo($_SESSION['tempsupervisor']);
		echo('"');
	}
	?>><br>
	<select class="input" class="selector" name="role">
	<option value="employee">Werknemer</option>
	<option value="supervisor">Manager</option>
	<option value="admin">Administrator</option>
	</select>
	<br>
	<br>
	<input class="inputButton fontRobotoBold" type="submit" value="Registreer"><br>
</form>

<?php

if(isset($_GET['usernameLength']) && filter_input(INPUT_GET, 'usernameLength', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
	echo('<p class="error">De gebruikersnaam moet tussen de ' . Config::NAMEMINLENGTH . ' en ' . Config::NAMEMAXLENGTH . ' karakters zijn</p>');
	echo('<br>');
}

if(isset($_GET['usernameExists']) && filter_input(INPUT_GET, 'usernameExists', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
	echo('<p class="error">Deze gebruikersnaam bestaat al</p>');
	echo('<br>');
}

if(isset($_GET['passwordLength']) && filter_input(INPUT_GET, 'passwordLength', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
	echo('<p class="error">Het wachtwoord moet tussen de ' . Config::PASSWORDMINLENGTH . ' en ' . Config::PASSWORDMAXLENGTH . ' karakters zijn</p>');
	echo('<br>');
}

if(isset($_GET['passwordConfirm']) && filter_input(INPUT_GET, 'passwordConfirm', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
	echo('<p class="error">De wachtwoorden komen niet overeen</p>');
	echo('<br>');
}

if(isset($_GET['nameLength']) && filter_input(INPUT_GET, 'nameLength', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
	echo('<p class="error">Uw naam moet tussen de  ' . Config::NAMEMINLENGTH . ' en ' . Config::NAMEMAXLENGTH . ' karakters zijn</p>');
	echo('<br>');
}

?>

<?php
	// TODO unset in beginning if register was successful
	unset($_SESSION['tempfirstName']);
	unset($_SESSION['templastName']);
	unset($_SESSION['tempusername']);
	unset($_SESSION['tempsupervisor']);
	unset($_SESSION['succesRegister']);
	unset($_SESSION['passwordDifference']);
	unset($_SESSION['nameExists']);
	unset($_SESSION['registerFailed']);
