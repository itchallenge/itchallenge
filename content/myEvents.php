<div class="fontFiraSans">
<h1 class="fontFiraSansTitle">Jouw aanvragen:</h1>
<?php
	$user = User::getInstance();

	$db = DB::getInstance();
	$userEvents = $db->getUserEvents($user->getId()); // array of events linked to user
?>
<table style="width:100%; border:1px solid black;">
	<tr>
		<th>
		naam aanvraag
		</th>
		<th>
		locatie
		</th>
		<th>
		start datum aanvrag
		</th>
		<th>
		eind datum aanvraag
		</th>
		<th>
		deadline
		</th>
		<th>
		prijs
		</th>
		<th>
		categorie
		</th>
		<th>
		goedgekeurd
		</th>
	</tr>
	<?php
		foreach($userEvents as $val) {
			// TODO improve get event by id function
			// only return one event
			$runningEvents = $db->getRunningEventById($val['runningEvent']);
			$runningEvent = $runningEvents[0];

			// TODO improve this get event by id function as well
			$events = $db->getEventById($runningEvent['event']);
			$event =  $events[0];

			$eventName = $event['event'];
			$location = $event['location'];
			$startEvent = $runningEvent['startEvent'];
			$endEvent = $runningEvent['endEvent'];
			$deadline = $runningEvent['deadline'];
			$price = $event['price'];
			$category = $runningEvent['category']; // only returns number
			$approved = $val['approved']; // boolean

			$categories = $db->getCategories();

			echo('<tr>');
			echo('<th>');
				echo($eventName);
			echo('</th>');
			echo('<th>');
				echo($location);
			echo('</th>');
			echo('<th>');
				echo($startEvent);
			echo('</th>');
			echo('<th>');
				echo($endEvent);
			echo('</th>');
			echo('<th>');
				echo($deadline);
			echo('</th>');
			echo('<th>');
				echo($price);
			echo('</th>');
			echo('<th>');
				foreach ($categories as $val) {
					if($val['id'] == $category) {
						echo($val['category']);
					}
				}
			echo('</th>');
			echo('<th>');
				if($approved === "1") {
					echo("ja");
				}
				else if($approved === "0") {
					echo("nee");
				}
			echo('</th>');
			echo('</tr>');
			/*
			event name
			location
			start
			end
			deadline
			price
			category
			approved
			*/
		}
		
	?>
</table>
</div>
