<h1 class="fontFiraSansTitle fontBlack">
	Unleash your potential: Nieuwe studieregeling bij Indicia in 2019
</h1>
<br>


<p class="fontFiraSans fontBlack">
	Jouw teamleider heeft het al met je besproken: Indicia heeft een nieuwe studieregeling.
	Concreet houdt dit in dat je 5% van je werktijd mag spenderen aan ontwikkeling op het gebied van technische skills & projectmanagement, sales en marketing & communicatie. 
	Heb je ideeën over jouw ontwikkeling, dien dan een verzoek in bij je leidinggevende, om samen te kijken tot welke concrete afspraken jullie kunnen komen.
</p>
<br>

<p class="fontFiraSans fontBlack">
	Wil je gebruik maken van deze regeling?
</p>

<p class="fontFiraSans fontBlack">
	Log dan in op deze webpagina en vul het formulier in/aan. 
	Je persoonlijke inlog met (voorlopig) wachtwoord heb je al ontvangen.
	Je hoort dan zo snel als mogelijk of je leidinggevende akkoord is of niet.
</p>

<br>

<p class="fontFiraSans fontBlack">
	Vragen hierover? Loop even naar je leidinggevende of naar PZ.
</p>

<br>

<?php
require(Config::RESOURCESPATH . 'login.php');
