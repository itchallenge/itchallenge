<div class="fontFiraSans">
<h1 class="fontFiraSansTitle">Aanvragen werknemers:</h1>
<?php
	$user = User::getInstance();

	$db = DB::getInstance();

	$username = $user->getUsername();

	$users = $db->getUsers(array(
		'supervisor' => $username,
	));

?>
<table style="width:100%; border:1px solid black;">
	<?php
		foreach($users as $userf) {


			$userEvents = $db->getUserEvents($userf['id']); // array of events linked to user
			foreach($userEvents as $val) {
				// TODO improve get event by id function
				// only return one event
				$runningEvents = $db->getRunningEventById($val['runningEvent']);
				$runningEvent = $runningEvents[0];

				// TODO improve this get event by id function as well
				$events = $db->getEventById($runningEvent['event']);
				$event =  $events[0];

				$eventName = $event['event'];
				$location = $event['location'];
				$startEvent = $runningEvent['startEvent'];
				$endEvent = $runningEvent['endEvent'];
				$deadline = $runningEvent['deadline'];
				$price = $event['price'];
				$category = $runningEvent['category']; // only returns number
				$approved = $val['approved']; // boolean

				$categories = $db->getCategories();

				echo('<tr>');
				echo('<th>');
					echo($eventName);
				echo('</th>');
				echo('<th>');
					echo($location);
				echo('</th>');
				echo('<th>');
					echo($startEvent);
				echo('</th>');
				echo('<th>');
					echo($endEvent);
				echo('</th>');
				echo('<th>');
					echo($deadline);
				echo('</th>');
				echo('<th>');
					echo($price);
				echo('</th>');
				echo('<th>');
					foreach ($categories as $val) {
						if($val['id'] == $category) {
							echo($val['category']);
						}
					}
				echo('</th>');
				echo('<th>');
					if($approved === "1") {
						echo("ja");
					}
					else if($approved === "0") {
						echo("nee");
					}
				echo('</th>');
				echo('<th>');
				if($approved == '0') {
					echo('
						<form action="' . Config::RESOURCESPATH . 'approveCode.php" method="post">
							<input type="text" id="userid" name="userid" value="'. $userf['id'] .'">
							<input type="text" id="runningEventId" name="runningEventId" value="' . $runningEvent['id'] . '">
							<input type="text" id="approve" name="approve" value="1">
							<input class="inputButton" type="submit" id="submit" name="submit" value="Keur goed">
						</form>
					');
				} else if($approved == '1') {
					echo('
					<form action="' . Config::RESOURCESPATH . 'approveCode.php" method="post">
						<input type="text" id="userid" name="userid" value="'. $userf['id'] .'">
						<input type="text" id="runningEventId" name="runningEventId" value="' . $runningEvent['id'] . '">
						<input type="text" id="approve" name="approve" value="0">
						<input class="inputButton" type="submit" id="submit" name="submit" value="Keur af">
					</form>
				');
				}
				echo('</th>');
				echo('</tr>');
				/*
				event name
				location
				start
				end
				deadline
				price
				category
				approved
				*/
			}
		}
	?>
</table>
</div>
