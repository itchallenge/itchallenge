<div class="fontFiraSans" id="textHome">
<h1 class="fontFiraSansTitle" >Ontwikkeling en Innovatie<h1>
<br>
Welkom
<?php 
	$user = USER::getInstance();
	echo($user->getFirstName() . ' ' . $user->getLastName());
?>
,
<br>
<br>
Op deze site kan je jouw extra ontwikkelingen gaan aangeven. 
<br>
Wil je een boek lezen voor marketing & organisatie?
<br>
Of ga je een cursus volgen voor skills & projectmanagment? 
<br>
Dit en vele andere keuzes kan je invoeren onder het tablad Aanvraag. 
<br>
Hier kan je je aanmelden voor een al bestaande cursus, of je kan een compleet nieuwe cursus aanvragen. 
<br>
Jouw aanvraag wordt doorgestuurd naar jouw leidinggevende, 
<?php 
	$Supervisor = USER::getInstance();
	echo($Supervisor->getSupervisor());
?>
, deze zal jouw extra onwikkeling goed- of afkeuren.
<br>
 Dit bericht ontvang je onder het tablad berichten.	
<br>
<br>
Vragen hierover? Loop even naar je leidinggevende of naar PZ.
<br>
<br>

</div>
<div id="imageOntwikkeling">
<img src="<?php echo(Config::IMAGEPATH);?>ontwikkeling.png" id="ontwikkeling">
</div>
