<div id="accountWrapper">
	<script src="<?php echo(Config::JAVASCRIPTPATH)?>account.js"></script>
	<div id="accountImageWrapper">
		<img SRC="<?php echo(Config::IMAGEPATH);?>accountfoto.jpg" id="accountImage">
	</div>
	<form class="fontFiraSans" id="account" action="<?php echo(Config::RESOURCESPATH);?>changeAccount.php" method="POST">
		<h1 class="fontFiraSansTitle" float: centre >Account</h1>
		<br>
		<?php 
			$user = User::getInstance();
		?>

		<label for="first_name">Voornaam: </label>
		<input id="first_name" type="text" name="first_name"
		<?php
			echo('value="');
			echo($user->getFirstName());
			echo('"');
		?>><br>

		<label for="last_name">Achternaam: </label>
		<input id="last_name" type="text" name="last_name"
		<?php
			echo('value="');
			echo($user->getLastName());
			echo('"');
		?>><br>

		<label for="username">Gebruikersnaam: </label>
		<input id="username" type="text" name="username" disabled="true"
		<?php
			echo('value="');
			echo($user->getUsername());
			echo('"');
		?>><br>

		<label for="supervisor">Leidinggevende: </label>
		<input id="supervisor" type="text" name="supervisor" disabled="true"
		<?php
			echo('value="');
			echo($user->getSupervisor());
			echo('"');
		?>><br>

		<label for="role">rol: </label>
		<input id="role" type="text" name="role" disabled="true"
		<?php
			echo('value="');
			echo($user->getRole());
			echo('"');
		?>><br>
		<input type="submit" name="saveAccount" id="saveAccount" value="Opslaan" class="inputButton">
	</form>
</div>
