<?php 
require_once 'resources/init.php';
$user = User::getInstance();

if($user->isLogin()) {
	$pageBuilder = new PageBuilder();
	$pageBuilder->buildPage(array(
		'title' => 'Mijn aanvragen',
		'content' => 'myEvents.php',
		'css' => 'myEvents.php',
		'headerImage' => 'indicialogo_wit.png',
	));
} else {
	header('Location: index.php');
}
