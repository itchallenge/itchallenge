<?php 
require_once 'resources/init.php';
$user = User::getInstance();

if($user->isLogin() && $user->getRole() === 'supervisor' || $user->getRole() === 'admin') {
	$pageBuilder = new PageBuilder();
	$pageBuilder->buildPage(array(
		'title' => 'Aanvragen werknemers',
		'content' => 'approve.php',
		'css' => 'approve.php',
		'headerImage' => 'indicialogo_wit.png',
	));
} else {
	header('Location: index.php');
}
