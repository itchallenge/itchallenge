<?php
class Config {
	// Configure options and rename this file Config.php
	
	// Charset for head, set to UTF-8
	const CHARSET = "UTF-8";
	// Authors in head
	const AUTHOR = "Tim Leerssen, Teun van der Wolf, Rik Engel, Meindert Kempe";
	// Default title in head
	const TITLE = "Title";
	
	// include_path set to the root of this project
	// const INCLUDEPATH = "/home/$username/http";
	const INCLUDEPATH = "";
	
	// Default theme
	const THEME = "light.php";
	const CSSPATH = "css/";
	
	// default layout file locations
	const HEAD = "headTemplate.php";
	const BODY = "bodyTemplate.php";
	const HEADER = "headerTemplate.php";
	const HEADERIMAGE = "indicialogo_zwart.png";
	const PAGETEMPLATE = "pageTemplate.php";
	// Default content for resources (head, body etc)
	const RESOURCESPATH = "resources/";
	// Default page content
	const CONTENT = "index.php";
	// Default folder for content
	const CONTENTPATH = "content/";
	// Default icon
	const ICON = "icon.png";
	// Default image path
	const IMAGEPATH = "img/";
	// Default javascript path
	const JAVASCRIPTPATH = "js/";
	
	// MySql database settings
	const DBHOST = '127.0.0.1';
	const DBDATABASE = '';
	const DBPORT = '3306';
	const DBCHARSET = 'utf8';
	const DBUSERNAME = 'root';
	const DBPASSWORD = '';
	const USERTABLE = 'users';
	const USERNAME = 'username';
	const FIRSTNAME = 'first_name';
	const LASTNAME = 'last_name';
	const SUPERVISOR = 'supervisor';
	const PASSWORDHASH = 'password_hash';
	const ROLE = 'role';
	const USERID = 'id';
	const NAMEMINLENGTH = 2;
	const NAMEMAXLENGTH = 45;
	const PASSWORDMINLENGTH = 4;
	const PASSWORDMAXLENGTH = 512;

	// category table names
	const CATEGORYTABLE = 'category';
	const EVENTSTABLE = 'events';
	const RUNNINGEVENTSTABLE = 'runningEvents';
	const USEREVENTSTABLE = 'userEvents';
}
