<?php
require_once 'resources/init.php';
$user = User::getInstance();

if(!$user->isLogin()) {
	header('Location: index.php');
}
$pageBuilder = new PageBuilder();
$pageBuilder->buildPage(array(
	'title' => 'Home',
	'content' => 'home.php',
	'css' => 'home.php',
	'headerImage' => 'indicialogo_wit.png',
));
