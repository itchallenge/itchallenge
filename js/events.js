"use strict"

window.onload = function() {
	// TODO fix event descriptions

	window.runningEventCategoryPicker = document.getElementById('runningEventCategoryPicker');
	window.runningEventPicker = document.getElementById('runningEventPicker');


	window.newEventCheckbox = document.getElementById('newEventCheckbox');

	window.eventCategoryPicker = document.getElementById('eventCategoryPicker');
	window.eventPicker = document.getElementById('eventPicker');

	window.startEvent = document.getElementById('startEvent');
	window.endEvent = document.getElementById('endEvent');
	window.deadline = document.getElementById('deadline');


	window.eventCheckbox = document.getElementById('eventCheckbox');

	window.eventName = document.getElementById('eventName');
	window.Location = document.getElementById('eventLocation');
	window.eventPrice = document.getElementById('eventPrice');


	// Load event categories into picker as soon as possible
	loadEventCategories();
	loadEvents(1);
	loadRunningEvents(1);


	// Set event listener for checkbox
	window.newEventCheckbox.addEventListener('change', updateNewEventCheckbox);
	window.eventCheckbox.addEventListener('change', updateEventCheckbox);
	// Update checkbox code
	updateEventCheckbox();
	updateNewEventCheckbox();

	window.eventCategoryPicker.addEventListener('change', updateEvents);
	window.runningEventCategoryPicker.addEventListener('change', updateRunningEvents)

	// Load events into event picker as soon as page loads

}

function updateNewEventCheckbox() {

	let runningEventCategoryPicker = window.runningEventCategoryPicker;
	let runningEventPicker = window.runningEventPicker;


	let newEventCheckbox = window.newEventCheckbox;

	let eventCategoryPicker = window.eventCategoryPicker;
	let eventPicker = window.eventPicker;

	let startEvent = window.startEvent;
	let endEvent = window.endEvent;
	let deadline = window.deadline;


	let eventCheckbox = window.eventCheckbox;

	let eventname = window.eventName;
	let location = window.Location;
	let eventprice = window.eventPrice;

	if(newEventCheckbox.checked) {
		runningEventCategoryPicker.disabled = true;
		runningEventPicker.disabled = true;

		eventCategoryPicker.disabled = false;
		eventPicker.disabled = false;

		startEvent.disabled = false;
		endEvent.disabled = false;
		deadline.disabled = false;

		eventCheckbox.disabled = false;

		eventname.disabled = false;
		location.disabled = false;
		eventprice.disabled = false;

		updateEventCheckbox()

	} else {
		runningEventCategoryPicker.disabled = false;
		runningEventPicker.disabled = false;

		eventCategoryPicker.disabled = true;
		eventPicker.disabled = true;

		startEvent.disabled = true;
		endEvent.disabled = true;
		deadline.disabled = true;

		eventCheckbox.disabled = true;

		eventname.disabled = true;
		location.disabled = true;
		eventprice.disabled = true;
	}
}


function updateEventCheckbox() {

	//let eventCategoryPicker = window.eventCategoryPicker;
	let eventPicker = window.eventPicker;

	let startEvent = window.startEvent;
	let endEvent = window.endEvent;
	let deadline = window.deadline;


	let eventCheckbox = window.eventCheckbox;

	let eventname = window.eventName;
	let location = window.Location;
	let eventprice = window.eventPrice;

	if(eventCheckbox.checked) {
		eventPicker.disabled = true;
		eventname.disabled = false;
		location.disabled = false;
		eventprice.disabled = false;
		//eventInput.disabled = false;
	} else {
		eventPicker.disabled = false;
		eventname.disabled = true;
		location.disabled = true;
		eventprice.disabled = true;
		//eventInput.disabled = true;
	}

}

function updateEvents() {
	loadEvents(window.eventCategoryPicker.value);
}

function updateRunningEvents() {
	loadRunningEvents(window.runningEventCategoryPicker.value);
}

function loadEventCategories() {

	// Do post request for categories
	postRequest('resources/getEvents.php', 'request=getCategories').then(function(data) {
		// Loop through data and add to categories
		for (const key in data) {
			let categoryOption = document.createElement('option');
			categoryOption.value = key;
			categoryOption.text = data[key];
			window.runningEventCategoryPicker.add(categoryOption);
			window.eventCategoryPicker.add(categoryOption.cloneNode(true));
		}
	});
}

function loadEvents(category) {

	// Do post request for events
	postRequest('resources/getEvents.php', 'request=getEvents&category=' + category).then(function(data) {
		// Loop through data and add to picker
		//console.log(data);
		window.eventPicker.innerHTML = '';
		data.forEach(element => {
			let eventOption = document.createElement('option');
			eventOption.value = element['id'];
			eventOption.text = element['event'] + ' locatie: ' + element['location'] + ' prijs: €' + element['price'];
			window.eventPicker.add(eventOption);
		});
	});
}

function loadRunningEvents(category) {

	// Do post request for running events
	postRequest('resources/getEvents.php', 'request=getRunningEvents&category=' + category).then(function(data) {
		// Loop through data and add to picker
		//console.log(data);
		window.runningEventPicker.innerHTML = '';
		for (const key in data) {
			data.forEach(element => {
				let eventOption = document.createElement('option');
				eventOption.value = element['id'];
				eventOption.text = element['event'] + ' start: ' + element['startEvent'] + ' eind: ' + element['endEvent'] + ' deadline: ' + element['deadline'];
				window.runningEventPicker.add(eventOption);
			});
		}
	});
}

function postRequest(url, data) {
	return fetch(url, {
		method: "POST",
		credentials: "same-origin",
		headers: {
			"Content-Type": "application/x-www-form-urlencoded",
		},
		redirect: "follow",
		body: data,
	})
	.then(function(response){
		return response.json()
	});
}
