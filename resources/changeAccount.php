<?php
chdir('../');
require_once 'resources/init.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){


	// sanitize input
	$firstName = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$lastName = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	//$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	//$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	//$confirmPassword = filter_input(INPUT_POST, 'password_confirm', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	
	// Set session to temporarily remember username
	//$_SESSION['tempusername'] = $username;

	// Register user or get errors

	$user = User::getInstance();

	if($user->getRole() !== 'admin') {

		$id = $user->getId();
		
		User::changeAccount(array(
			'id' => $id,
			'firstName' => $firstName,
			'lastName' => $lastName,
			// 'username' = $username,
			// 'supervisor' = $supervisor,
			// 'originalPassword' = $originalPassword,
			// 'password' = $password,
			// 'confirmPassword' = $confirmPassword,
			// 'role' = $role,
			// 'department' = $department,
			// 'workingHoursWeek' = $workingHoursWeek,
		));
	}

	header('Location: ../account.php');
}
