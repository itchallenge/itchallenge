<!DOCTYPE html>
<html>
	<?php 
	// Include Head
	if($this->use['head'] !== false) {
		$this->buildElement('head');
	}
	?>
	
	<?php
	// Include Body
	if($this->use['body'] !== false) {
		$this->buildElement('body');
	}
	?>
</html>
