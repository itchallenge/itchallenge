<?php
class ElementBuilder {
	
	// define default values
	protected $head = Config::HEAD;
	protected $body = Config::BODY;
	protected $title = Config::TITLE;
	protected $theme = Config::THEME;
	protected $css;
	protected $cssPath = Config::CSSPATH;
	protected $content = Config::CONTENT;
	protected $contentPath = Config::CONTENTPATH;
	protected $resourcesPath = Config::RESOURCESPATH;
	protected $icon = Config::ICON;
	protected $imagePath = Config::IMAGEPATH;
	protected $header = Config::HEADER;
	protected $headerImage = Config::HEADERIMAGE;


	// class constructor
	public function __construct()
	{
		
	}
	
	// retrieve page head
	public function getHead() {
		return $this->head;
	}
	
	// set head for page and head construction
	public function setHead($file) {
		$this->head = $file;
	}
	
	// retrieve page body
	public function getBody() {
		return $this->body;
	}
	
	// set body for page and body construction
	public function setBody($file) {
		$this->body = $file;
	}

	// retrieve page header
	public function getHeader() {
		return $this->header;
	}

	// set header
	public function setHeader($file) {
		$this->header = $file;
	}

	// retrieve page header
	public function getHeaderImage() {
		return $this->headerImage;
	}

	// set header
	public function setHeaderImage($file) {
		$this->headerImage = $file;
	}

	// retrieve page content
	public function getContent() {
		return $this->content;
	}
	// set page content
	public function setContent($file) {
		$this->content = $file;
	}
	// get current content path
	public function getContentPath() {
		return $this->contentPath;
	}
	// set current content path
	public function setContentPath($path) {
		$this->contentPath = $path;
	}
	
	// get and set title, only works for default head or custom head with 
	// <title><?php if(isset($this->title)) { echo($this->title); } ></title>
	public function getTitle() {
		return $this->title;
	}
	
	public function setTitle($title) {
		$this->title = $title;
	}
	
	// get and set theme, only works for default head or custom head with 
	// 	<?php if(isset($this->theme)) {echo('<link rel="stylesheet" href="{$this->theme}">');} >
	public function getTheme() {
		return $this->theme;
	}
	
	public function setTheme($file) {
		$this->theme = $file;
	}
	
	// get and set page specific css, only works for default head or custom head with 
	// 	<?php if(isset($this->theme)) {echo('<link rel="stylesheet" href="{$this->theme}">');} >
	public function getCss() {
		return $this->css;
	}

	public function setCss($path) {
		$this->css = $path;
	}
	
	// Set and get path of css folder
	public function getCssPath() {
		return $this->cssPath;
	}
	
	public function setCssPath($path) {
		$this->cssPath = $path;
	}
	// Set and get path of resources folder
	public function getResourcesPath() {
		return $this->resourcesPath;
	}
	
	public function setResourcesPath($path) {
		$this->resourcesPath = $path;
	}
	
	// Set and get path of image folder
	public function getImagePath() {
		return $this->imagePath;
	}
	
	public function setImagePath($path) {
		$this->imagePath = $path;
	}
	
	// Set and get image file
	public function getIcon() {
		return $this->icon;
	}
	
	public function setIcon($file) {
		$this->icon = $file;
	}
	
	public function buildElement($element, $var = false) {
		
		$getFunction = 'get' . $element;

		// If $var is given set the properties to $var
		if($var !== false) {
			$currentVal = $this->$getFunction();
			$setFunction = 'set' . $element;
			$this->$setFunction($var);
		}
		
		// Check if the element can be build using this class
		if(isset($this->$element)) {
			// Autput the correct element
			switch($element) {
				case 'title':
					echo('<title>' . $this->$getFunction() . '</title>');
					break;
				case 'icon':
					echo('<link rel="icon" href="' . $this->getImagePath() . $this->$getFunction() . '">');
					break;
				case 'theme':
					echo '<link rel="stylesheet" href="' . $this->getCssPath() . $this->$getFunction() . '">';
					break;
				case 'css':
					echo '<link rel="stylesheet" href="' . $this->getCssPath() . $this->$getFunction() . '">';
					break;
				case 'content':
					require $this->getContentPath() . $this->$getFunction();
					break;
				case 'headerImage':
					$test = '<img src="' . $this->getImagePath() . $this->$getFunction() . '" id="headerImage">';
					echo $test;
					break;
				default:
					require $this->getResourcesPath() . $this->$getFunction();
			}

		} else {
			// If the element can not be build using the class throw an exception
			throw new Exception('Element "' . $element . '" is not a valid element.');
		}

		// Reset value to saved value if it was changed
		if($var !== false) {
			$this->$setFunction($currentVal);
		}
	}
}

