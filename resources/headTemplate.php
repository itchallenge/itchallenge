<head>
	<meta charset="<?php echo(Config::CHARSET) ?>">
	<meta name="author" content="<?php echo(Config::AUTHOR)?>">
	<?php 
	// Echo title if set
	if($this->use['title'] !== false) {
		$this->buildElement('title');
	}
	?>
	<!-- TODO replace hardcoded path-->
	<link rel="stylesheet" href="css/common.php">
	<?php
	// Theme specific stylesheet loading
	if($this->use['theme'] !== false) {
		$this->buildElement('theme');
	}
	// Page specific stylesheet loading
	if($this->use['css'] !== false && $this->getCss() !== null) {
		$this->buildElement('css');
	}
	if($this->use['icon'] !== false && $this->use['imagePath'] !== false && $this->getIcon() !== null) {
		$this->buildElement('icon');
	}
	?>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Roboto:300,400,700|Fira+Sans:300,400&ver=f5b6cc59e0bc3ac0739eb5d7334fca9b" rel="stylesheet">
</head>
