<div id="login" class="fontFiraSans">
	<form action="<?=Config::RESOURCESPATH?>loginCode.php" method="post" id="loginForm">
		<input type="text" name="username" placeholder="Gebruikersnaam" class="input"
		<?php
		if(isset($_SESSION['tempusername'])) {
			echo('value="');
			echo($_SESSION['tempusername']);
			echo('"');
		}
		?>><br>
		<input type="password" name="password" placeholder="Wachtwoord" class="input"> <br>
		<input type="submit" value="Login" class="inputButton fontRobotoBold">
	</form>
	<?php
	if(isset($_GET['loginError']) && filter_input(INPUT_GET, 'loginError', FILTER_SANITIZE_FULL_SPECIAL_CHARS) == true) {
		echo('<p class="error">De gebruikersnaam of wachtwoord is fout</p>');
	}
	?>
	<?php
		// TODO unset in beginning if login was successful
		unset($_SESSION['tempusername']);
	?>
</div>
