<?php
header('Content-Type: application/json');
chdir('../');
require_once 'resources/init.php';

$user = User::getInstance();

if($_SERVER['REQUEST_METHOD'] === 'POST' && $user->isLogin()){

	// Sanitize input
	$request = filter_input(INPUT_POST, 'request', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	// Get datbase instance
	$db = DB::getInstance();

	$result = array();
	// TODO return user linked to event (for runningEvent)
	// TODO return correct name for running event

	// Check what to get
	if($request === 'getCategories') {
		$categories = $db->getCategories();

		foreach($categories as $var) {
			//array_push($categories, $var[0]);
			$result[$var['id']] = $var['category'];
		}
	} else if($request === 'getEvents') {
		// Sanitize input
		$category = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		// Get events
		$events = $db->getEvents($category);

		foreach($events as $key => $var) {
			$result[$key] = $events[$key];
		}
	} else if($request === 'getRunningEvents') {
		$category = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		// Get events
		$runningEvents = $db->getRunningEvents($category);

		foreach($runningEvents as $key => $var) {
			$result[$key] = $runningEvents[$key];
		}
	}
	echo json_encode($result);
}
