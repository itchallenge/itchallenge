<body>
	<div id="wrapper" class="container">
		<div id="headerWrapper">
			<div id="header">
				<?php
				if($this->use['resourcesPath'] !== false && $this->use['header'] !== false) {
					$this->buildElement('header');
				}
				?>
			</div>
		</div>
		<div id="contentWrapper">
			<div id="content">
				<?php
				if($this->use['contentPath'] !== false && $this->use['content'] !== false) {
					$this->buildElement('content');
				}
				?>	
			</div>
		</div>
		<div id="footerWrapper">
			<div id="footer">
				<?php
				require(Config::RESOURCESPATH . 'footerTemplate.php');
				?>
			</div>
		</div>
	</div>
</body>
