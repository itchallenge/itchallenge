<?php
	if($this->use['imagePath'] !== false && $this->use['headerImage'] !== false && $this->getHeaderImage() !== null) {
		echo('<a href="index.php">');
		$this->buildElement('headerImage');
		echo('</a>');
	}
?>
<nav id="nav" class="fontRobotoBoldNav">
<?php
	$user = User::getInstance();
	if($user->isLogin()) {
		echo(
			'<a href="index.php" class="navButton">'.
			'Home'.
			'</a>');
		echo(
			'<a href="account.php" class="navButton">'.
			'Account'.
			'</a>');
		// if($user->getRole() === 'admin') {
		// 	echo(
		// 		'<a href="users.php" class="navButton">'.
		// 		'Gebruikers'.
		// 		'</a>');
		// }

		if($user->getRole() === 'admin') {
			echo(
				'<a href="register.php" class="navButton">'.
				'Nieuwe gebruiker'.
				'</a>');
		}
		
		if($user->getRole() === 'supervisor' || $user->getRole() === 'admin') {
			echo(
				'<a href="approve.php" class="navButton">'.
				'Aanvragen werknemers'.
				'</a>');
		}
		echo(
			'<a href="requestForm.php" class="navButton">'.
			'Niewe Aanvraag'.
			'</a>');	
		echo(
			'<a href="myEvents.php" class="navButton">'.
			'Mijn aanvragen'.
			'</a>');
	}
?>

<div id="logout">
<?php
	if($user->isLogin()) {
		echo(
			'<form action="' . Config::RESOURCESPATH . 'logoutCode.php" method="POST">'.
			'	<input type="submit" value="logout" class="inputButton fontRobotoBold logoutButton	">'.
			'</form>');
	}
?>
</div>
</nav>