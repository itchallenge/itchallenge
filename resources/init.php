<?php 

// Probably not needed, but if removed fix config loading
// Make certain include_path is set correctly
$includePathBak = get_include_path();
set_include_path(".:..");

// Require config
require_once 'config/Config.php';
// Set proper include path
set_include_path($includePathBak . ':' . Config::INCLUDEPATH);

// Register autoloader
spl_autoload_register(function($class) {
	require_once("resources/" . $class . ".php");
});

// start session
session_start();
