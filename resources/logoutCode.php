<?php
chdir('../');
require_once 'resources/init.php';

$user = User::getInstance();
if($user->isLogin()) {
	$user->logout();
}

if(isset($_SESSION['url'])){
	header('Location: ' . $_SESSION['url']);
} else {
	header('Location: ../index.php');
}