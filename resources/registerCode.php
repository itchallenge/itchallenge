<?php
chdir('../');
require_once 'resources/init.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){


	// sanitize input
	$firstName = filter_input(INPUT_POST, 'first_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$lastName = filter_input(INPUT_POST, 'last_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$confirmPassword = filter_input(INPUT_POST, 'password_confirm', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$supervisor = filter_input(INPUT_POST, 'supervisor', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	
	// Set session to temporarily remember username
	$_SESSION['tempusername'] = $username;

	
	if(empty($supervisor)) {
		$supervisor = '';
	}

	// Register user or get errors
	$errors = User::register($firstName, $lastName, $username, $password, $confirmPassword, $supervisor, $role);

	// generate url to pass errors back to page
	$url = '../register.php';
	if(!$errors['success']) {
		unset($errors['success']);
		$url .= '?';
		foreach($errors as $error => $value) {
			$url .= $error . '=' . $value . '&';
		}
		$url = substr($url, 0, -1);

		// Set session to remember info in case of invalid input
		$_SESSION['tempfirstName'] = $firstName;
		$_SESSION['templastName'] = $lastName;
		$_SESSION['tempsupervisor'] = $supervisor;
		$_SESSION['temprole'] = $role;

	} else {
		$url = '../index.php';
	}

	header('Location: ' . $url);
}
