<?php
class User {
	private static $instance;
	private $username;
	private $firstName;
	private $lastName;
	private $supervisor;
	private $role;
	private $login = false;
	private $id;

	private function __construct() {
		if(isset($_SESSION['firstName']) && isset($_SESSION['lastName'])
		&& isset($_SESSION['username']) && isset($_SESSION['supervisor'])
		&& isset($_SESSION['role']) && isset($_SESSION['login'])) {
			$this->firstName = $_SESSION['firstName'];
			$this->lastName = $_SESSION['lastName'];
			$this->username = $_SESSION['username'];
			$this->supervisor = $_SESSION['supervisor'];
			$this->role = $_SESSION['role'];
			$this->id = $_SESSION['userid'];
			$this->login = $_SESSION['login'];
		}
	}

	private function __clone() {
		
	}

	// getInstance for singleton class
	public static function getInstance() {
		// If there is not jet an instance of the object instantiate one
		if(!isset(static::$instance)) {
			static::$instance = new static;
		}
		// Return the instance
		return static::$instance;
	}

	public function login($username, $password){
		$db = DB::getInstance();
		$users = $db->getUsers(array(
			Config::USERNAME => $username
		));

		$errors = array();

		// Check if a single user has been returned an check that users password
		if(count($users) === 1 && $users[0][Config::PASSWORDHASH] === crypt($password, $users[0][Config::PASSWORDHASH])) {
			$this->setFirstName($users[0][Config::FIRSTNAME]);
			$this->setLastName($users[0][Config::LASTNAME]);
			$this->setUsername($users[0][Config::USERNAME]);
			$this->setSupervisor($users[0][Config::SUPERVISOR]);
			$this->setRole($users[0][Config::ROLE]);
			$this->setId($users[0][Config::USERID]);
			$this->setLogin(true);

			$errors["success"] = true;
		} else {
			$errors['success'] = false;
			$errors["loginError"] = true;
		}

		return $errors;
	}

	public function logout() {
		// unset all user specific variables
		$this->setUsername(null);
		$this->setFirstName(null);
		$this->setLastName(null);
		$this->setSupervisor(null);
		$this->setRole(null);
		$this->setId(null);
		$this->setLogin(false);
	}

	public static function register($firstName, $lastName, $username, $password, $passwordConfirm, $supervisorID = "-1", $role, $length = 32, $strong = true, $rounds = 10000) {
		
		$errors = array();

		// do validation checks and save all errors
		$errors = array_merge($errors, Validate::username($username));
		$errors = array_merge($errors, Validate::name($firstName, $lastName));
		$errors = array_merge($errors, Validate::password($password, $passwordConfirm));
		// TODO validate role?

		// TODO get supervisor ID if neccesary
		// think about how to implement that, keep in mind client side

		// register if there are no errors
		if(empty($errors)) {
			// get cryptographically safe random bytes for salt
			$random = base64_encode(openssl_random_pseudo_bytes($length, $strong));

			// generate salt
			$salt = substr($random, 0, 16);

			// hash password using salt and sha512
			$passHash = crypt($password, '$6$rounds=' . $rounds . '$' . $salt . '$');

			// get database instance
			$db = DB::getInstance();
			// make registration query
			$db->register($firstName, $lastName, $username, $supervisorID, $passHash, $role);
			// report succes
			$errors['success'] = true;
			return $errors;
		} else {
			// report error
			$errors['success'] = false;
			return $errors;
		}
	}

	public static function changeAccount($data = array()) {
		// $errors = User::changeAccount(array(
		// 	'firstName' = $firstName,
		// 	'lastName' = $lastName,
		// 	'username' = $username,
		// 	'supervisor' = $supervisor,
		// 	'originalPassword' = $originalPassword,
		// 	'password' = $password,
		// 	'confirmPassword' = $confirmPassword,
		// 	'role' = $role,
		// 	'department' = $department,
		// 	'workingHoursWeek' = $workingHoursWeek,
		// ));

		// UPDATE table_name
		// SET column1 = value1, column2 = value2, ...
		// WHERE condition;

		// pass user and values to db update function

		$db = DB::getInstance();
		$db->updateName($data['id'], $data['firstName'], $data['lastName']);
		

		$user = User::getInstance();
		if($data['id'] === $user->getId()) {
			// update session variables
			if(isset($data['firstName'])) {
				$user->setFirstName($data['firstName']);
			}
			if(isset($data['lastName'])) {
				$user->setLastName($data['lastName']);
			}
		}

	}

	/**
	 * Get the value of username
	 */ 
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * Get the value of firstName
	 */ 
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Get the value of lastName
	 */ 
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Get the value of supervisor
	 */ 
	public function getSupervisor()
	{
		return $this->supervisor;
	}

	/**
	 * Get the value of role
	 */ 
	public function getRole()
	{
		return $this->role;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of username
	 */ 
	private function setUsername($username)
	{
		$this->username = $username;
		$_SESSION['username'] = $username;
	}

	/**
	 * Set the value of firstName
	 */ 
	private function setFirstName($firstName)
	{
		$this->firstName = $firstName;
		$_SESSION['firstName'] = $firstName;
	}

	/**
	 * Set the value of lastName
	 */ 
	private function setLastName($lastName)
	{
		$this->lastName = $lastName;
		$_SESSION['lastName'] = $lastName;
	}

	/**
	 * Set the value of supervisor
	 */ 
	private function setSupervisor($supervisor)
	{
		$this->supervisor = $supervisor;
		$_SESSION['supervisor'] = $supervisor;
	}

	/**
	 * Set the value of role
	 */ 
	private function setRole($role)
	{
		$this->role = $role;
		$_SESSION['role'] = $role;
	}

	/**
	 * Set the value of id
	 */ 
	private function setId($id)
	{
		$this->id = $id;
		$_SESSION['userid'] = $id;
	}

	/**
	 * Set the value of login
	 */ 
	private function setLogin($login)
	{
		$this->login = $login;
		$_SESSION['login'] = $login;
	}

	public function isLogin() {
		return $this->login;
	}
}
