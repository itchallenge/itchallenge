<?php 
class Validate {

	public static function username($username) {
		$errors = array();

		if(!static::usernameLength($username)){
			$errors['usernameLength'] = true;
		}
		if(!static::existingUsername($username)) {
			$errors['usernameExists'] = true;
		}

		return $errors;
	}

	private static function usernameLength($username) {
		return (strlen($username) >= Config::NAMEMINLENGTH && strlen($username) <= Config::NAMEMAXLENGTH) ? true : false;
	}
	
	private static function existingUsername($username) {
		$confirm = true;
		// connect to the database
		$db = DB::getInstance();
		// check if username is already in use
		$existingUsers = $db->getUsers(array(Config::USERNAME => $username));
		// TODO improve this
		foreach($existingUsers as $user) {
			if($user[Config::USERNAME] == $username){
				$confirm = false;
			}
		}
		return $confirm;
	}

	public static function password($password, $passwordConfirm) {
		$errors = array();

		if(!static::passwordLength($password)) {
			$errors['passwordLength'] = true;
		}
		if(!static::passwordConfirm($password, $passwordConfirm)) {
			$errors['passwordConfirm'] = true;
		}

		return $errors;
	}

	private static function passwordLength($password) {
		return (strlen($password) >= Config::PASSWORDMINLENGTH && strlen($password) <= Config::PASSWORDMAXLENGTH) ? true : false;
	}

	private static function passwordConfirm($password, $passwordConfirm) {
		return $password === $passwordConfirm ? true : false;
	}

	public static function name($firstName, $lastName) {
		$errors = array();
		if(!static::nameLength($firstName) || !static::nameLength($lastName)) {
			$errors['nameLength'] = true;
		}

		return $errors;
	}

	private static function nameLength($name) {
		return (strlen($name) >= Config::NAMEMINLENGTH && strlen($name) <= Config::NAMEMAXLENGTH) ? true : false;
	}
}
