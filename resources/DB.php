<?php
class DB {
	
	// Initiate variables
	private static $instance;
	private $PDO;
	private $dsn;
	private $dbusername;
	private $dbpassword;
	private $userTable = Config::USERTABLE;
	private $username = Config::USERNAME;
	private $firstName = Config::FIRSTNAME;
	private $lastName = Config::LASTNAME;
	private $supervisor = Config::SUPERVISOR;
	private $passwordHash = Config::PASSWORDHASH;
	private $role = Config::ROLE;
	private $categoryTable = Config::CATEGORYTABLE;
	private $eventsTable = Config::EVENTSTABLE;
	private $runningEventsTable = Config::RUNNINGEVENTSTABLE;
	private $userEventsTable = Config::USEREVENTSTABLE;

	// Protected contructor for singleton class
	private function __construct() {
		// Define the variables needed for the PDO object
		$this->dsn = 'mysql:dbname=' . Config::DBDATABASE . ';host=' . Config::DBHOST . ';port=' . Config::DBCHARSET . ';charset=' . Config::DBCHARSET;
		$this->dbusername = Config::DBUSERNAME;
		$this->dbpassword = Config::DBPASSWORD;
		
		
		try {
			// Try to create a PDO object
			$this->PDO = new PDO($this->dsn, $this->dbusername, $this->dbpassword, array());
		} catch(PDOException $e) {
			// If a PDO object cannot be instantiated throw an exception
			throw new Exception("Error connecting to database.");
		}
		// Set some attributes of the PDO object
		$this->PDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		$this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	// Protected clone for singleton class
	private function __clone() {
		
	}
	
	// getInstance for singleton class
	public static function getInstance() {
		// If there is not jet an instance of the object instantiate one
		if(!isset(static::$instance)) {
			static::$instance = new static;
		}
		// Return the instance
		return static::$instance;
	}
	
	// Query function
	private function query($query, $vars = array()) {
		// Get PDO object
		$pdo = $this->PDO;
		// Prepare statement
		$sql = $pdo->prepare($query);
		// Execute statement
		$sql->execute($vars);
		// Return the results
		try {
			return $sql->fetchAll();
		}catch(PDOException $e){
			return true;
		}
	}

	public function register($firstName, $lastName, $username, $supervisor, $passwordHash, $role){

		// construct query
		// TODO add proper role support
		$query = "INSERT INTO $this->userTable ($this->firstName, $this->lastName, $this->username, $this->supervisor, $this->passwordHash, $this->role) 
		VALUES (:firstName, :lastName, :username, :supervisor, :passwordHash, :role);";
		// add variables to array
		$vars = array(
			':firstName' => $firstName,
			':lastName' => $lastName,
			':username' => $username,
			':supervisor' => $supervisor,
			':passwordHash' => $passwordHash,
			':role' => $role,
		);
		// execute query
		$this->query($query, $vars);
	}

	public function getUsers(array $checks) {
		// construct query
		// format data like so:
		// array(
		// 'first_name = ' => 'name'
		// 'last_name = ' => 'name'
		// );
		$query = "SELECT * FROM $this->userTable where ";
		
		// create array for variables in query
		$vars = array();
		$first = true;
		foreach($checks as $column => $var) {
			if(!$first) {
				$query .= ' AND ';
			}
			$first = false;	
			$query .= $column . ' = ?';
			array_push($vars, $var);
		}
		
		$result = $this->query($query, $vars);
		return $result;
	}

	public function getCategories() {
		// construct query
		$query = "SELECT * FROM $this->categoryTable";
		
		// return result
		return $this->query($query);

	}

	public function addEvent($event) {

		$query = "INSERT INTO $this->eventsTable (event, location, price, category) 
		VALUES (:event, :location, :price, :category);";

		$this->query($query, $event);

		$eventid = $this->PDO->lastInsertId();
		return $eventid;
	}

	// TODO Redesign getEvent functions
	public function getEvents($category) {
		// construct query
		// TODO extend config for category column
		// TODO unify get event functions
		$query = "SELECT * FROM $this->eventsTable where $this->categoryTable = ?";

		$vars = array($category);

		// return result
		return $this->query($query, $vars);
	}

	public function addRunningEvent($event) {

		$query = "INSERT INTO $this->runningEventsTable (event, startEvent, endEvent, deadline, category) 
		VALUES (:event, :startEvent, :endEvent, :deadline, :category);";

		$this->query($query, $event);

		$eventid = $this->PDO->lastInsertId();
		return $eventid;
	}

	public function getRunningEvents($category) {
		// construct query
		// TODO extend config for category column
		// TODO unify get event functions
		$query = "SELECT * FROM $this->runningEventsTable where $this->categoryTable = ?";
		
		$vars = array($category);

		// return result
		return $this->query($query, $vars);
	}

	public function getEventById($id) {
		// construct query
		// TODO extend config for id
		// TODO unify get event functions
		$query = "SELECT * FROM $this->eventsTable where id = ?";

		$vars = array($id);

		// return result
		return $this->query($query, $vars);
	}

	public function getRunningEventById($id) {
		// construct query
		// TODO extend config for id
		// TODO unify get event functions
		$query = "SELECT * FROM $this->runningEventsTable where id = ?";
		
		$vars = array($id);

		// return result
		return $this->query($query, $vars);
	}

	public function getUserEvents($userid) {
		// construct query
		// TODO add username column to config
		$query = "SELECT * FROM $this->userEventsTable where userid = ?";
		
		$vars = array();
		array_push($vars, $userid);

		// return result
		return $this->query($query, $vars);
		
		//TODO add uservents to the thing above :)
	}

	// TODO further abstract selection statments

	public function getUserEvent($userid, $runningEventId) {

		$query = "SELECT * FROM $this->userEventsTable where userid = :userid AND runningEvent = :runningEventId";

		$vars = array(
			':userid' => $userid,
			':runningEventId' => $runningEventId,
		);
		
		return $this->query($query, $vars);
	}
	
	public function addUserEvent($userid, $runningEventId) {

		$var = array(
			':userid' => $userid,
			':runningEvent' => $runningEventId,
		);

		$query = "INSERT INTO $this->userEventsTable (userid, runningEvent) 
		VALUES (:userid, :runningEvent);";

		$this->query($query, $var);

	}

	public function updateApproved($userid, $runningEvent, $approved) {

		$vars = array(
			':userid' => $userid,
			':runningEvent' => $runningEvent,
			':approved' => $approved,
		);

		$query = "UPDATE $this->userEventsTable SET approved = :approved WHERE userid = :userid and runningEvent = :runningEvent";

		$this->query($query, $vars);
	}

	public function updateName($id, $firstName, $lastName) {

		$vars = array(
			':id' => $id,
			':firstName' => $firstName,
			':lastName' => $lastName,
		);

		$query = "UPDATE $this->userTable SET $this->firstName = :firstName, $this->lastName = :lastName WHERE id = :id";

		$this->query($query, $vars);
	}
	
}
