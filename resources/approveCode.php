<?php
chdir('../');
require_once 'resources/init.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	$userid = filter_input(INPUT_POST, 'userid', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$runningEventId = filter_input(INPUT_POST, 'runningEventId', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$approve = filter_input(INPUT_POST, 'approve', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	//$newEventCheckbox = filter_input(INPUT_POST, 'newEventCheckbox', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	$db = DB::getInstance();

	$db->updateApproved($userid, $runningEventId, $approve);

	header('Location: ../approve.php');
} else {
	header('Location: ../index.php');
}

// runningEventCategory
// runningEvent
// newEventCheckbox
// eventCategory
// event
// eventCheckbox
// eventInput
// submit