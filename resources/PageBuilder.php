<?php
class PageBuilder extends ElementBuilder {
	
	protected $pageTemplate = Config::PAGETEMPLATE;

	// Define possible properties
	protected $use = array(
		'title' => true,
		'content' => true,
		'contentPath' => true,
		'css' => true,
		'cssPath' => true,
		'theme' => true,
		'head' => true,
		'body' => true,
		'header' => true,
		'headerImage' => true,
		'pageTemplate' => true,
		'resourcesPath' => true,
		'icon' => true,
		'imagePath' => true
	);
	
	// Get and set pageTemplate
	public function getPageTemplate() {
		return $this->pageTemplate;
	}
	public function setPageTemplate($file) {
		$this->pageTemplate = $file;
	}

		// construct a page
	public function buildPage($param = array() ) {
		
		// Cycle through and set each property
		foreach ($param as $key => $value) {
			
			// TODO split up this block into functions
			// Check for existance of property
			if(isset($this->use[$key])) {
				
				if(is_bool($value)) {
					// If it is a boolean just set the use value
					$this->use[$key] = $value;
				} else {
					// If it is not a boolean set the use value to true and
					// set the properties value to the given value
					$this->use[$key] = true;
					$function = 'set' . $key;
					$this->$function($value);
				}
			} else {
				// If the property does not exist throw an error
				throw new Exception('Property "' . $key . '" does not exist');
			}
		}
		
		// Output the page
		require $this->getResourcesPath() . $this->getPageTemplate();
		
		// Reset all properties to true
		foreach($this->use as $key => $value) {
			$this->use[$key] = true;
		}
	}
	
}
