<?php
chdir('../');
require_once 'resources/init.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){

	// sanitize input
	$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	
	// set session in order to not lose everything in case of invalid input
	$_SESSION['tempusername'] = $username;
	
	$user = User::getInstance();

	// Login user or get errors
	$errors = $user->login($username, $password);
	// $_SESSION['successLogin'] = true;

	// generate url to pass errors back to page
	$url = '../index.php';
	if(!$errors['success']) {
		unset($errors['success']);
		$url .= '?';
		foreach($errors as $error => $value) {
			$url .= $error . '=' . $value . '&';
		}
		$url = substr($url, 0, -1);
	}

	header('Location: ' . $url);
}
