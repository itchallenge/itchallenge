<?php
chdir('../');
require_once 'resources/init.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	$runningEventCategory = filter_input(INPUT_POST, 'runningEventCategory', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$runningEvent = filter_input(INPUT_POST, 'runningEvent', FILTER_SANITIZE_FULL_SPECIAL_CHARS);


	$newEventCheckbox = filter_input(INPUT_POST, 'newEventCheckbox', FILTER_SANITIZE_FULL_SPECIAL_CHARS);


	$eventCategory = filter_input(INPUT_POST, 'eventCategory', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$event = filter_input(INPUT_POST, 'event', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	
	$startEvent = filter_input(INPUT_POST, 'startEvent', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$endEvent = filter_input(INPUT_POST, 'endEvent', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$deadline = filter_input(INPUT_POST, 'deadline', FILTER_SANITIZE_FULL_SPECIAL_CHARS);


	$eventCheckbox = filter_input(INPUT_POST, 'eventCheckbox', FILTER_SANITIZE_FULL_SPECIAL_CHARS);


	$eventName = filter_input(INPUT_POST, 'eventName', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$eventLocation = filter_input(INPUT_POST, 'eventLocation', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
	$eventPrice = filter_input(INPUT_POST, 'eventPrice', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

	// TODO figure out when to get database instance

	if(!isset($newEventCheckbox))
	{
		$user = User::getInstance();
		$db = DB::getInstance();

		// TODO switch to id
		$dbUsers = $db->getUsers(array(
			Config::USERNAME => $user->getUsername(),
		));
		//$events = $db->getRunningEventById($runningEvent);
		$events = $db->getRunningEventById($runningEvent);


		if($events != null && $dbUsers != null) {
			$dbUser = $dbUsers[0];
			$event = $events[0];
			$existingEvents = $db->getUserEvent($dbUser['id'], $event['id']);

			if($existingEvents != null) {
				// TODO insert into database

				$db->addUserEvent($dbUser['id'], $event['id']);
			}

		}

	} else if(isset($newEventCheckbox) && !isset($eventCheckbox)) {
		// New event
		$user = User::getInstance();
		$db = DB::getInstance();

		// TODO switch to id
		$dbUsers = $db->getUsers(array(
			Config::USERNAME => $user->getUsername(),
		));

		$events = $db->getEventById($event);

		// TODO do proper checks here
		if($events != null && $dbUsers != null) {
			$dbUser = $dbUsers[0];
			$eventf = $events[0];

			// TODO do proper checks here

			// create running event

			$runningEvent = array(
				':event' => $eventf['id'],
				':startEvent' => $startEvent,
				':endEvent' => $endEvent,
				':deadline' => $deadline,
				':category' => $eventCategory
			);

			// insert into running events

			$runningEventId = $db->addRunningEvent($runningEvent);

			// insert into userevents
			
			$db->addUserEvent($dbUser['id'], $runningEventId);

		}


		// insert into database

	} else if(isset($newEventCheckbox) && isset($eventCheckbox)) {
		// completely new event
		$user = User::getInstance();
		$db = DB::getInstance();

		// TODO switch to id
		$dbUsers = $db->getUsers(array(
			Config::USERNAME => $user->getUsername(),
		));
		// echo 'start:';
		// echo $startEvent;
		// echo '<br>';
		// echo 'end:';
		// echo '<br>';
		// echo $endEvent;
		// echo '<br>';
		// echo 'deadline:';
		// echo $deadline;
		if($dbUsers != null) {
			$dbUser = $dbUsers[0];
			// TODO do proper checks here

			// create event

			$event = array(
				':event' => $eventName,
				':location' => $eventLocation,
				':price' => $eventPrice,
				':category' => $eventCategory,
			);

			// insert into events

			$eventId = $db->addEvent($event);

			// create running event
			
			$runningEvent = array(
				':event' => $eventId,
				':startEvent' => $startEvent,
				':endEvent' => $endEvent,
				':deadline' => $deadline,
				':category' => $eventCategory
			);

			// insert into running events

			$runningEventId = $db->addRunningEvent($runningEvent);

			// insert into userevents
			
			$db->addUserEvent($dbUser['id'], $runningEventId);
		}
	}
	header('Location: ../myEvents.php');
} else {
	header('Location: ../index.php');
}

// runningEventCategory
// runningEvent
// newEventCheckbox
// eventCategory
// event
// eventCheckbox
// eventInput
// submit