<br>
<h1 class="fontRobotoTitle textWhite">Indicia</h1>
<br>
<p class="fontFiraSans textWhite">
	Elke moderne organisatie is een digitale organisatie. Cloud, apps, mobility, robotisering, Big Data, 3D Printing, In-Memory computing; de technische mogelijkheden groeien met de dag. Exponentieel zelfs, want op de snijvlakken van al deze ontwikkelingen ontstaat een haast duizelingwekkende hoeveelheid mogelijkheden en kansen.
</p>

