Please do not contribute changes directly to the master branch, contribute to development 
instead and create a merge request when features are ready to be pushed to master.
