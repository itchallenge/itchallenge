<?php
require_once 'resources/init.php';

$user = User::getInstance();

if($user->isLogin()) {
	header('Location: home.php');
}
$pageBuilder = new PageBuilder();
$pageBuilder->buildPage(array(
	'title' => 'Login',
	'contentPath' => Config::RESOURCESPATH,
	'content' => 'login.php'
));
