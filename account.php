<?php 
require_once 'resources/init.php';
$user = User::getInstance();

if(!$user->isLogin()) {
	header('Location: home.php');
}
// construct default page
$pageBuilder = new PageBuilder();
$pageBuilder->buildPage(array(
	'content' => 'account.php',
	'title' => 'Account',
	'css' => 'account.php',
	'headerImage' => 'indicialogo_wit.png'
));
