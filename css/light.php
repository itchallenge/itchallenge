<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php

$indiciaWhite = '#f3f3f3';
$indiciaPink = '#bd5493' ;
$white = '#ffffff';
$indiciaRed = '#ef2447';
$black = '#000000';
$errorRed = '#cc0000';
$inputTextColor = '#555555';
$inputBorderColor = '#e0e1e1';
$inputBackgroundColor = '#fbfcfc';
$inputButtonBackgroundColor = '#ef2447';
$inputButtonBackgroundColorHover = '#ff2447';
$inputButtonBackgroundColorActive = '#df2447';

$logoutButtonBackgroundColor = '#cf2447';
$logoutButtonBackgroundColorHover = '#ff2447';
$logoutButtonBackgroundColorActive = '#df2447';

$navTextColor = '#7d7d7d';
?>

/*
<style>
/**/

#header, #headerWrapper {
	background-color: <?=$indiciaWhite?>;
}

#content, #contentWrapper {
	background-color: <?=$white?>;
}

#footer, #footerWrapper {
	background-color: <?=$indiciaPink?>;
}

.textWhite {
	color: <?=$white?>;
}

.textBlack {
	color: <?=$black?>;
}

.error {
	color: <?=$errorRed?>;
}

.input {
	color: <?=$inputTextColor?>;
	border-color: <?=$inputBorderColor?>;
	background-color: <?=$inputBackgroundColor?>;
}

.inputButton {
	background-color: <?=$inputButtonBackgroundColor?>;
	color: <?=$white?>;
}

.inputButton:hover {
	background-color: <?=$inputButtonBackgroundColorHover?>;
}

.inputButton:active {
	background-color: <?=$inputButtonBackgroundColorActive?>;
}

.logoutButton {
	background-color: <?=$logoutButtonBackgroundColor?>;
}

.logoutButton:hover {
	background-color: <?=$logoutButtonBackgroundColorHover?>;
}

.logoutButton:active {
	background-color: <?=$logoutButtonBackgroundColorActive?>;
}

.navButton:before {
	background-color: <?=$indiciaRed?>;
}

.navButton {
	color: <?=$navTextColor?>;
}
