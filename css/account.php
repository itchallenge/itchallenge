<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php

$indiciaWhite = '#f3f3f3';
$indiciaRed ='#ef2447' ;
$white = "#ffffff";

?>


/*
<style>
/**/

#header, #headerWrapper {
	background-color: <?=$indiciaRed?>;
}

.navButton {
	color: <?=$white?>;
}

.navButton:before {
	background-color: <?=$indiciaWhite?>;
}

#account {
	text-align: right;
}

#accountWrapper {
	display: flex;
}

#accountImage {
	height: auto;
	width: 100%;
}

#accountImageWrapper {
	height: 100%;
	width: 100%;
	justify-content:center;
	max-height: 20%;
	max-width: 20%;
	margin-right: 5%;
}


@media (orientation: landscape) {
	#accountWrapper {
		margin-top: 5%;
		margin-left: 5%;
	}
}
