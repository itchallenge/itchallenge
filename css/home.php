<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php

$indiciaWhite = '#f3f3f3';
$indiciaRed ='#ef2447' ;
$white = "#ffffff";

?>

/*
<style>
/**/

#header, #headerWrapper {
	background-color: <?=$indiciaRed?>;
}

.navButton {
	color: <?=$white?>;
}

.navButton:before {
	background-color: <?=$indiciaWhite?>;
}

#content {
	display:flex;
}

#ontwikkeling {
	height: auto;
	width: 100%;
}

#imageOntwikkeling {
	height: 100%;
	width: 100%;
	justify-content:right;
	max-height: 45%;
	max-width: 45%;
	margin-right: 0%;
	overflow-x:hidden;
	overflow-y:hidden;
	margin-top: 10%;
	margin-left: 10%;
	}
	
}
