<?php header("Content-type: text/css; charset: UTF-8"); ?>

/*
<style>
/**/

* {
	margin: 0;
	padding: 0;
	font-size: 1em;
	font: inherit;

	overflow: auto;
	box-sizing: border-box;
}

html,body {
	height: 100%;
	width: 100%;
}

#wrapper {
	min-height: 100%;
	min-width: 100%;
	word-spacing: normal;
	font-style: normal;
	font-variant: normal;
}

.container {
	display: grid;
	grid-template-columns:repeat(6, 1fr);
	grid-template-rows:115px repeat(6, 1fr) 350px;
}

#headerWrapper, #contentWrapper, #footerWrapper {
	display: grid;
	grid-template-columns:repeat(6, 1fr);
	grid-template-rows: 1fr;
	grid-column-start: 1;
	grid-column-end: -1;
}

#headerWrapper {
	grid-row-start: 1;
	grid-row-end: 2;
	z-index: 1000;
	box-shadow: 0px 2px 10px 0px #bbb;
}

#contentWrapper {
	grid-row-start: 2;
	grid-row-end: 8;
}

#footerWrapper {
	grid-row-start: 8;
	grid-row-end: 9;
}

#header, #content, #footer {
	grid-column-start: 1;
	grid-column-end: -1;
}

#header {
	display: flex;
	align-items: center;
	padding: 1%;
}

#headerImage {
	height: 32px;
	width: 150px;
	max-height: 100%;
	max-width: 100%;	
}

#content {
	padding: 1%;
}

.fontFiraSansTitle {
	font: normal 300 36px / 44px "Fira Sans", Helvetica, Arial, Verdana, sans-serif;
}

.fontFiraSans {
	font: normal 300 14px / 26px "Fira Sans", Helvetica, Arial, Verdana, sans-serif;
}

.fontRobotoTitle {
	font: normal 300 36px / 44px "Roboto", Helvetica, Arial, Verdana, sans-serif;
}

.fontRobotoBold {
	font: normal bold 14px / 40px "Roboto", Helvetica, Arial, Verdana, sans-serif;
}

.fontRobotoBoldNav {
	font: normal bold 16px / 20px "Roboto", Helvetica, Arial, Verdana, sans-serif;
}

.input {
	border-radius: 4px;
	padding: 5px 4px;
	border: 1px solid;
	font-size: 150%;
}

.inputButton {
	padding-left: 13.333333333333px;
	padding-right: 13.333333333333px;
	border-radius: 3px;
	border: none;
	font-size: 150%;
}

#nav {
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: flex-end;
	box-sizing: border-box;
	margin-left: 5%;
}

.navButton {
	display: flex;
	align-items: center;
	position: relative;
	color: #000;
	text-decoration: none;
	height: 40px;
	margin-right: 4%;
}

.navButton:before {
	content: "";
	position: absolute;
	width: 100%;
	height: 2px;
	bottom: 0px;
	left: 0;
	opacity: 0;
	transform: translateY(0);
	transition: all 0.3s;
}

.navButton:hover:before {
	opacity: 100;
	transform: translateY(-5px);
}

@media (min-width: 1500px) {
	.container {
		grid-template-columns:repeat(8, 1fr);
		grid-template-rows:115px repeat(6, 1fr) 200px;
	}
	
	#header, #content, #footer {
		grid-column-start:2;
		grid-column-end: -2;
		margin: 0%;
	}

	#contentWrapper {
		grid-row-start: 2;
		grid-row-end: 8;
	}
	#footerWrapper {
		grid-row-start: 8;
		grid-row-end: 9;
	}
	.input {
		font-size: 100%;
	}

	.inputButton {
		font-size: 100%;
	}
}

