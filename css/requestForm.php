<?php header("Content-type: text/css; charset: UTF-8"); ?>

<?php

$lightblue = 'lightblue';
$darkblue = 'darkblue';
$green = 'green';
$indiciaWhite = '#f3f3f3';
$indiciaRed ='#ef2447' ;
$white = "#ffffff";

?>

/*
<style>
/**/

.selector {
	width:75%;
}

.label {
	min-width: 50%;
	display: inline-block;
}

#header, #headerWrapper {
	background-color: <?=$indiciaRed?>;
}

.navButton {
	color: <?=$white?>;
}

.navButton:before {
	background-color: <?=$indiciaWhite?>;
}
