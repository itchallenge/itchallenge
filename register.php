<?php
require_once 'resources/init.php';
$user = User::getInstance();

if($user->isLogin() && $user->getRole() === 'admin') {
	$pageBuilder = new PageBuilder();
	$pageBuilder->buildPage(array(
		'title' => 'Register',
		'content' => 'register.php',
		'css' => 'register.php',
		'headerImage' => 'indicialogo_wit.png'
	));
} else {
	header('Location: index.php');
}