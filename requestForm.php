<?php
require_once 'resources/init.php';

$user = User::getInstance();

if($user->isLogin()) {
	$pageBuilder = new PageBuilder();
	$pageBuilder->buildPage(array(
		'title' => 'Aanvraag',
		'content' => 'requestForm.php',
		'css' => 'requestForm.php',
		'headerImage' => 'indicialogo_wit.png',
	));
} else {
	header('Location: index.php');
}
